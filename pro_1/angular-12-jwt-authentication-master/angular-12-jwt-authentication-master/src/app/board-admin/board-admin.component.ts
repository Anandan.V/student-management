import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../_services/Employee';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css'],
})
export class BoardAdminComponent implements OnInit {
  employee: Employee[] = [];

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getemployeeslist();
  }
  private getemployeeslist() {
    this.userService.getAdminBoard().subscribe((data) => {
      this.employee = data;
    });
  }
  updateById(id: number) {
    this.router.navigate(['/update', id]);
  }
  deleteById(id: number) {
    this.userService.deleteById(id).subscribe(() => {
      this.getemployeeslist();
    });
  }
}
