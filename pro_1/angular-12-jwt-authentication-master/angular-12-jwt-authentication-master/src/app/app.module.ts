import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardUserComponent } from './board-user/board-user.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { FooterComponent } from './footer/footer.component';
import { UpdateComponent } from './update/update.component';
import { CreateStudentComponent } from './create-student/create-student.component';
import { StafflistComponent } from './stafflist/stafflist.component';
import { AllaccountComponent } from './allaccount/allaccount.component';
import { StaffaccountComponent } from './staffaccount/staffaccount.component';
import { StudentalltaskComponent } from './studentalltask/studentalltask.component';
import { UpdateStudentComponent } from './update-student/update-student.component';
import { UpdateStudentByModComponent } from './update-student-by-mod/update-student-by-mod.component';
import { StudentallmodComponent } from './studentallmod/studentallmod.component';
import { StaffAddComponent } from './staffaccount/staff-add/staff-add.component';
import { StaffUpdateComponent } from './staffaccount/staff-update/staff-update.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    FooterComponent,
    UpdateComponent,
    CreateStudentComponent,
    StafflistComponent,
    AllaccountComponent,
    StaffaccountComponent,
    StudentalltaskComponent,
    UpdateStudentComponent,
    UpdateStudentByModComponent,
    StudentallmodComponent,
    StaffAddComponent,
    StaffUpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
