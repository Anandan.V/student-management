import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';
import { Staffdata } from '../staffdata';

@Component({
  selector: 'app-staff-add',
  templateUrl: './staff-add.component.html',
  styleUrls: ['./staff-add.component.css'],
})
export class StaffAddComponent implements OnInit {
  staff: Staffdata = new Staffdata();

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    console.log(this.staff);
    this.saveData();
  }
  saveData() {
    this.userService.addStaff(this.staff).subscribe(
      (Data) => {
        this.goto();
      },
      (error) => console.log(error)
    );
  }
  goto() {
    this.router.navigate(['/stafflist']);
  }
}
