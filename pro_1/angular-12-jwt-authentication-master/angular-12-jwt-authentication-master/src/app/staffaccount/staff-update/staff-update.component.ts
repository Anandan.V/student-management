import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';
import { Staffdata } from '../staffdata';

@Component({
  selector: 'app-staff-update',
  templateUrl: './staff-update.component.html',
  styleUrls: ['./staff-update.component.css'],
})
export class StaffUpdateComponent implements OnInit {
  id!: number;
  staff: Staffdata = new Staffdata();

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.userService.getStaffById(this.id).subscribe((data) => {
      this.staff = data;
    });
  }

  saveData() {
    this.userService.updateStaffById(this.id, this.staff).subscribe((data) => {
      console.log(data);
      this.goto();
    });
  }
  goto() {
    this.router.navigate(['stafflist']);
  }
}
