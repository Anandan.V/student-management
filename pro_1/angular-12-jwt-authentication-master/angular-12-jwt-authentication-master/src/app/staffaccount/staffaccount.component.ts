import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { Staffdata } from './staffdata';

@Component({
  selector: 'app-staffaccount',
  templateUrl: './staffaccount.component.html',
  styleUrls: ['./staffaccount.component.css'],
})
export class StaffaccountComponent implements OnInit {
  stafflist: Staffdata[] = [];

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getlist();
  }
  private getlist() {
    this.userService.getStafflist().subscribe((data) => {
      this.stafflist = data;
    });
  }
  // updatestaff
  gotoUpdate(id: number) {
    this.router.navigate(['/update-staff', id]);
  }
  //deletestaff
  gotoDelete(id: number) {
    this.userService.deleteStaff(id).subscribe(
      (data) => {
        this.ngOnInit();
      },
      (error) => console.log(error)
    );
  }
}
