export class Staffdata {
  id!: number;
  staffname!: String;
  staffage!: number;
  qualifications!: String;
  joining_date!: String;
  department!: String;
  salary!: number;
}
