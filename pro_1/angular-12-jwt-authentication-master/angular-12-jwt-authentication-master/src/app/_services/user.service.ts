import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './Employee';
import { Student_Class } from './Student_Class';
import { Staffdata } from '../staffaccount/staffdata';

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  // getAdminBoard(): Observable<any> {
  //   return this.http.get(API_URL + 'admin', { responseType: 'text' });
  // }

  //getEmployeeAllList
  getAdminBoard(): Observable<Employee[]> {
    return this.http.get<Employee[]>(API_URL + 'admin');
  }

  getEmployeeById(id: number): Observable<Employee> {
    return this.http.get<Employee>(API_URL + 'admin/' + id);
  }

  deleteById(id: number): Observable<Object> {
    return this.http.delete(API_URL + 'admin/' + id);
  }

  updateEmployee(id: number, employee: Employee): Observable<Object> {
    return this.http.put(API_URL + 'admin/' + id, employee);
  }

  // student mapping
  getStudentAllData(): Observable<Student_Class[]> {
    return this.http.get<Student_Class[]>(API_URL + 'user');
  }
  getStudentById(id: number): Observable<Student_Class> {
    return this.http.get<Student_Class>(API_URL + 'admin/' + 'student/' + id);
  }
  createStudent(studentClass: Student_Class): Observable<object> {
    return this.http.post(API_URL + 'admin/' + 'createStudent', studentClass);
  }

  updateStudent(id: number, Student_Class: Student_Class): Observable<Object> {
    return this.http.put(API_URL + 'admin/' + 'student/' + id, Student_Class);
  }

  deleteByStudentId(id: number): Observable<Object> {
    return this.http.delete(API_URL + 'admin/' + 'student/' + id);
  }

  //Staff API's
  getStafflist(): Observable<Staffdata[]> {
    return this.http.get<Staffdata[]>(API_URL + 'Stafflist');
  }

  addStaff(Data: Staffdata): Observable<Object> {
    return this.http.post(API_URL + 'addstaff', Data);
  }

  deleteStaff(id: number): Observable<object> {
    return this.http.delete(API_URL + 'deleteStaff/' + id);
  }

  getStaffById(id: number): Observable<Staffdata> {
    return this.http.get<Staffdata>(API_URL + 'getStaffById/' + id);
  }

  updateStaffById(id: number, data: Staffdata): Observable<object> {
    return this.http.put(API_URL + 'updateStaffByid/' + id, data);
  }
}
