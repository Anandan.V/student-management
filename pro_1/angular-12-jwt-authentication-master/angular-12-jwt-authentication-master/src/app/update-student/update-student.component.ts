import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Student_Class } from '../_services/Student_Class';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css'],
})
export class UpdateStudentComponent implements OnInit {
  id!: number;
  students: Student_Class = new Student_Class();
  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.userService.getStudentById(this.id).subscribe(
      (data) => {
        this.students = data;
      },
      (error) => console.log(error)
    );
  }
  onSubmit() {
    this.userService.updateStudent(this.id, this.students).subscribe(
      (data) => {
        console.log(data);
        this.studentList();
      },
      (error) => console.log(error)
    );
  }
  studentList() {
    this.router.navigate(['/studentAll']);
  }
}
