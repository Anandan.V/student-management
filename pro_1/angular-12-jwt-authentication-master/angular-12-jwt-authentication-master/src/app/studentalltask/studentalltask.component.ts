import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student_Class } from '../_services/Student_Class';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-studentalltask',
  templateUrl: './studentalltask.component.html',
  styleUrls: ['./studentalltask.component.css'],
})
export class StudentalltaskComponent implements OnInit {
  student: Student_Class[] = [];
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getemployeeslist();
  }
  private getemployeeslist() {
    this.userService.getStudentAllData().subscribe(
      (data) => {
        this.student = data;
      },
      (err) => {
        this.student = JSON.parse(err.error).message;
      }
    );
  }
  updateStudentById(id: number) {
    this.router.navigate(['updateStudent', id]);
  }
  deleteById(id: number) {
    this.userService.deleteByStudentId(id).subscribe(() => {
      this.getemployeeslist();
    });
  }
}
