package com.cbt.sms.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stafflist")
public class Staff {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String staffname;
	private Integer staffage;
	private String qualifications;
	private String joining_date;
	private String department;
	private Integer salary; 
	
	
	public Staff() {
		
	}


	public Staff(String staffname, Integer staffage, String qualifications, String joining_date, String department,
			Integer salary) {
		
		this.staffname = staffname;
		this.staffage = staffage;
		this.qualifications = qualifications;
		this.joining_date = joining_date;
		this.department = department;
		this.salary = salary;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getStaffname() {
		return staffname;
	}


	public void setStaffname(String staffname) {
		this.staffname = staffname;
	}


	public Integer getStaffage() {
		return staffage;
	}


	public void setStaffage(Integer staffage) {
		this.staffage = staffage;
	}


	public String getQualifications() {
		return qualifications;
	}


	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}


	public String getJoining_date() {
		return joining_date;
	}


	public void setJoining_date(String joining_date) {
		this.joining_date = joining_date;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public Integer getSalary() {
		return salary;
	}


	public void setSalary(Integer salary) {
		this.salary = salary;
	}


		
	
	

}
